<?php

$secret = "6LcHIzAUAAAAAENsSRpfVAtuxW9oMqk2UT2jR-0l";
$result = false;
$response = $_POST["g-recaptcha-response"];

if($response != null)
{
$url = 'https://www.google.com/recaptcha/api/siteverify';
$data = array('secret' => $secret, 'response' => $response);

// use key 'http' even if you send the request to https://...
$options = array(
  'http' => array(
      'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
      'method'  => 'POST',
      'content' => http_build_query($data)
  )
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);
if ($result === FALSE) {
$myfile = fopen("log.txt", "w");
fwrite($myfile, $result);
fclose($myfile);
}
$result = json_decode($result, true);
}

if ($result["success"])
{
require 'class.phpmailer.php';
require 'class.smtp.php';


$subject = "Zpráva z kontaktního formuláře";
$body = "<p><strong>Jméno:</strong> ". $_POST['name']."</p>
  <p><strong>E-mail:</strong> ". $_POST['email'] ."</p>
  <p><strong>Zpráva:</strong><br><br> ". $_POST['message'] ."</p>";

$bodyWithoutFormating = "
  Jméno: ". $_POST['name'] ."
  E-mail: ". $_POST['email'] ."
  Zpráva: ". $_POST['message'];

$mail = new PHPMailer;
$mail->CharSet = "UTF-8";
$mail->setFrom($_POST['email'],$_POST['name']);
$mail->addAddress('info@creative-learning.cz', 'Tereza Warmanová');
$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = $subject;
$mail->Body    = $body;
$mail->AltBody = $bodyWithoutFormating;
$mail->WordWrap = 50;

if(!$mail->send()) {
    echo 'Email nemohl být odeslán.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'Email byl úspěšně odeslán.';

}
}
else{
  echo'Captcha nebyla ověřena, email nemohl být odeslán!';
}

?>
