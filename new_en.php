<?php include ('safe.php');?>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Creative-Learning > Admin</title>
  <meta name="author" content="Stuck-ups Webdesigns">
  <link rel="stylesheet" href="style.css">
  <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea',plugins: 'link image code', relative_urls: false, });</script>
</head>

<body>
<div id="wrapper">
  <header id="header"><a href="en/index.php" target="_blank"><img src="images/logo_creative_learning.png" alt="logo"></a></header>
  <nav><ul><li><a href="new_cs.php">Novinky CS</a></li><li><a href="new_en.php">Novinky EN</a></li><li><a href="logout.php">Odhlásit</a></li></ul></nav>
  <section>
  <h1>News content - English version</h1><hr>
<form method="post">
<textarea name="content" placeholder="News content here...">
<?php
$myfile = fopen("new_en.txt", "r") or die("Unable to open file!");
echo fread($myfile,filesize("new_en.txt"));
fclose($myfile);
?>
</textarea><br>
<button type="sumbit" name="save">Save changes</button>
</form><br>
<?php
if (isset($_POST["save"]))
{
	$myfile = fopen("new_en.txt", "w") or die("Unable to open file!");
	$txt = $_POST["content"];
	fwrite($myfile, $txt);
	fclose($myfile);
	echo "Changes Have been saved!";
}
?>
  </section>

  <footer>2017 © Tereza Warmanová-Vožniaková <br>Created by <a href="http://www.stuck-ups.com
</body>
</html>
