<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
<base href="../" />
  <title>Creative-Learning > Home</title>
  <meta name="description" content="Welcome to Creative Language Learning. Find out more about the project and what is new.
Read academic and professional references.">
  <meta name="author" content="Tereza Warmanová-Vožniaková">
  <link rel="stylesheet" href="style.css">

</head>

<body>
<div id="wrapper">
  <header id="header"><a href="en/index.php"><img src="images/logo_creative_learning.png" alt="logo"></a></header>
  <nav><ul><li><a href="en/index.php">Home</a></li><li><a href="en/aboutme.html">About Me</a></li><li><a href="en/lessons.html">Lessons</a></li><li><a href="en/contactme.html">Contact Me</a></li></ul></nav>
  <section>

  <div id="submenu">
  Home:
  <ul>
  <li>
  <a href="en/index.php#welcome">Welcome</a>
  </li>
  <li>
  <a href="en/index.php#testimonials">Testimonials</a>
  </li>
  </ul><hr>
  <a href="en/index.php#header">&uarr; Back to top</a><br><br>
  <a href="index.php"><img class="icon" src="images/czech_republic_flag.gif" alt="Čeština"></a>&nbsp;
  <a href="en/index.php"><img class="icon" src="images/US-UK_flag.jpg" alt="English"></a>
  </div>
  <div class="row1"><div class="block1">
  <h1 id="welcome">Welcome</h1><hr>
<div class="portret"><img src="images/terezaportret_old.jpg" alt="Portrait of Tereza Warmanova-Vozniakova"><h5>Tereza Warmanová-Vožniaková</h5></div>
<div class="news"><h2>What's new:</h2><hr>
<ul>
<li><?php
$myfile = fopen("../new_en.txt", "r") or die("Unable to open file!");
echo fread($myfile,filesize("../new_en.txt"));
fclose($myfile);
?></li></ul></div>
  <br class="clear">

  <p class="italic">
  <h3 class="italic">Dear visitor,</h3><br>

Welcome to the Creative Language Learning project.<br>

Allow me to explain what the project is about and how it might be interesting for you. <br><span class="bold">Depending on what your needs are, select one category.</span><br><br>

<!--<p><span class="bold">1.</span> Are you interested in <span class="bold">cultivating</span> your <span class="bold">English</span> language <span class="bold">skills</span>? Is <span class="bold">critical</span> and <span class="bold">creative thinking</span> essential for you? Are you someone who has attended English language courses and found that course content is a bit too general? Would you like to <span class="bold">expand</span> your <span class="bold">knowledge</span> by exploring articles about other parts of the world? Would you like to learn how to see events from <span class="bold">other perspectives</span>? Would you like to <span class="bold">develop a growth mindset</span> and <span class="bold">self-reflection</span>?
<br><br>
If you answered <span class="bold">yes</span> to most of the questions, please go to <a href="lessons.html#cll">Lessons - CLL Lessons</a>.
</p>
<p><hr>!-->
1. Do you want to <span class="bold">revise</span> your <span class="bold">grammar</span></span> and <span class="bold">use of tenses</span>? Do you need to <span class="bold">speak better and understand</span> quite <span class="bold">quickly</span>? Do you need to <span class="bold">learn English</span> for your <span class="bold">work</span>?<br>
Are you trying to <span class="bold">improve</span> your English skills and you need one-to-one teaching?
<br><br>
If you answered <span class="bold">yes</span> to most of the questions, please go to  <a href="en/lessons.html#individual">Lessons - Individual Lessons</a>.

<p><hr>
2. Do you want to <span class="bold">revise</span> your <span class="bold">grammar</span></span> and <span class="bold">use of tenses</span>? Do you need to <span class="bold">speak better and understand</span> quite <span class="bold">quickly</span>? Do you need to <span class="bold">learn English</span> for your <span class="bold">work</span>?
  Are you looking for <span class="bold">flexible online</span> lessons or <span class="bold">lessons one-to-one</span>?<br>
Are you <span class="bold">motivated</span> by other learners? Do you enjoy <span class="bold">speaking</span> to other students <span class="bold">in English</span>? Do you like <span class="bold">group work</span> or <span class="bold">pair work</span>?
<br><br>
If you answered <span class="bold">yes</span> to most of the questions, please go to  <a href="en/lessons.html#group">Lessons - Group Courses</a>.




  </div></div>
  <div class="row2"><div class="block2">
  <h1 id="testimonials">Testimonials</h1><hr>
<h2>Academic reference</h2>

<div class="refer"><p class="italic">„Tereza’s communication skills in English, both verbal and written, are excellent. She is also a highly motivated student who completes all assignments on time. The quality of Tereza’s work is also outstanding, as her excellent marks indicate.”</p>

<span class="bold">Dr. Steve Roe</span><br>

ENGL 100 (Academic Writing), Northern Lights College, Fort St. John, BC, Canada</div><hr>
<div class="refer">
<p class="italic">“Tereza is a very good writer, an excellent communicator and a diligent worker. In addition, she has made positive contributions to our work as a class. Her writing ability stretches across genres skillfully from short and long report writing and business memos to web presentations and peer feedback, breaking down complex ideas and concepts and stating them in clear, succinct prose… In addition to her writing and critical thinking skills, Tereza combines interpersonal aptitudes that make her easy to work with and she gives and receives feedback in a positive manner. She also demonstrates a continual willingness to help others with considerate, diplomatic comments on their work.”</p>

<span class="bold">John Vigna, MFA</span><br>

CMNS 251 (Professional Report Writing), University of the Fraser Valley, Abbotsford, BC, Canada</div><hr>




<h2>Professional reference</h2>
<div class="refer">
<p class="italic">„Když jsem se rozhodla začít s angličtinou, kterou jsem ve škole nikdy neměla, vystřídala jsem několik
kurzů a několik lektorů, než jsem se seznámila s Terezou Warmanovou. Teprve s ní jsem zažila pocit,
že jsem konečně setřásla nálepku věčného začátečníka, že mě učení baví, že jsem se trochu víc
rozmluvila a že se angličtinu nebojím použít. Tereza je příjemná, trpělivá lektorka, schopná
přizpůsobit lekci žákovi, jeho schopnostem, ale i zájmům a momentální náladě. Nikdy nedá najevo
netrpělivost nebo rozmrzelost z opakujících se chyb. Na její hodiny se těším a odcházím pozitivně
naladěná, s chutí číst si knížky v angličtině a sledovat filmy bez dabingu.”</p>

<span class="bold">MUDr. Alena Mazáková</span></div>
<hr>
<div class="refer">
<p class="italic">„Na přístupu Terezy k výuce angličtiny si vážím hlavně toho, že poté co poznala, co na mě platí,
  tak se tomu přizpůsobila. Hodiny byly rozmanité a využité do maxima. Když jsme se nemohly sejít osobně, spojily jsme se přes skype -
  byla jsem překvapená, jaké finty na výuku na dálku má. Přestože jsem z časových důvodů (já vím, výmluva..) lekce přerušila, ráda si i
  nyní na facebooku čtu její sdílené příspěvky.”</p>

<span class="bold">Romana Musilová, AmbitionPro</span></div><hr> </div>

  </section>

  <footer>2018 © Tereza Warmanová-Vožniaková <br>Created by <a href="http://www.stuck-ups.com">Stuck-ups Web Designs</a></footer>
</div>
</body>
</html>
