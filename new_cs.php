<?php include ('safe.php');?>
<html lang="cs">
<head>
  <meta charset="utf-8">

  <title>Creative-Learning > Admin</title>
  <meta name="author" content="Stuck-ups Webdesigns">
  <link rel="stylesheet" href="style.css">
  <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea',plugins: 'link image code', relative_urls: false, });</script>
</head>

<body>
<div id="wrapper">
  <header id="header"><a href="index.php" target="_blank"><img src="images/logo_creative_learning.png" alt="logo"></a></header>
  <nav><ul><li><a href="new_cs.php">Novinky CS</a></li><li><a href="new_en.php">Novinky EN</a></li><li><a href="logout.php">Odhlásit</a></li></ul></nav>
  <section>
  <h1>Obsah novinky - Česká verze</h1><hr>
<form method="post">
<textarea name="content" placeholder="Obsah novinek zde...">
<?php
$myfile = fopen("new_cs.txt", "r") or die("Unable to open file!");
echo fread($myfile,filesize("new_cs.txt"));
fclose($myfile);
?>
</textarea><br>
<button type="sumbit" name="save">Uložit změny</button>
</form><br>
<?php
if (isset($_POST["save"]))
{
	$myfile = fopen("new_cs.txt", "w") or die("Unable to open file!");
	$txt = $_POST["content"];
	fwrite($myfile, $txt);
	fclose($myfile);
	echo "Changes Have been saved!";
}
?>
  </section>

  <footer>2017 © Tereza Warmanová-Vožniaková <br>Created by <a href="http://www.stuck-ups.com">Stuck-ups Web Designs</a></footer>
</div>
</body>
</html>
