<!doctype html>

<html lang="cs">
<head>
  <meta charset="utf-8">
  <title>Creative-Learning > Domů</title>
  <meta name="description" content="Vítejte v Kreativní výuce jazyků (Creative Language Learning). Zjistěte více o projektu a co je
v současné době nového. Přečtěte si akademické a profesní reference.">
  <meta name="author" content="Tereza Warmanová-Vožniaková">
  <link rel="stylesheet" href="style.css">
</head>

<body>
<div id="wrapper">
  <header id="header"><a href="index.php"><img src="images/logo_creative_learning.png" alt="logo"></a></header>
  <nav><ul><li><a href="index.php">Domů</a></li><li><a href="omne.html">O mně</a></li><li><a href="lekce.html">Lekce</a></li><li><a href="kontakt.html">Kontakt</a></li></ul></nav>
  <section>

  <div id="submenu">
  Domů:
  <ul>
  <li>
  <a href="#welcome">Vítejte</a>
  </li>
  <li>
  <a href="#testimonials">Doporučení</a>
  </li>
  </ul><hr>
  <a href="#header">&uarr; Nahoru</a><br><br>
  <a href="index.php"><img class="icon" src="images/czech_republic_flag.gif" alt="Čeština"></a>&nbsp;
  <a href="en/index.php"><img class="icon" src="images/US-UK_flag.jpg" alt="English"></a>
  </div>
  <div class="row1"><div class="block1">
  <h1 id="welcome">Vítejte</h1><hr>
<div class="portret"><img src="images/terezaportret_old.jpg" alt="Portrait of Tereza Warmanova-Vozniakova"><h5>Tereza Warmanová-Vožniaková</h5></div><div class="news"><h2>Co je nového:</h2><hr><ul><li>
<?php
$myfile = fopen("new_cs.txt", "r") or die("Unable to open file!");
echo fread($myfile,filesize("new_cs.txt"));
fclose($myfile);
?></li></ul></div>
  <br class="clear">

  <p class="italic">
  <h3 class="italic">Vážený návštěvníku,</h3><br>

vítejte v Kreativní výuce jazyků (Creative Language Learning). Dovolte, abych Vám představila tento projekt a vysvětlila, jak by mohl být pro Vás zajímavý. Nejprve vyberte jednu z níže psaných kategorií dle Vašich potřeb.<br><br>

<!--<p><span class="bold">1.</span>	Zajímalo by Vás jak byste mohl/a  <span class="bold">kultivovat Vaši angličtinu</span>? Je <span class="bold">kritické a kreativní myšlení </span> pro Vás zásadní? Navštěvoval/a jste kurz anglického jazyka a přišla Vám náplň kurzu moc obecná? Chtěli byste si <span class="bold">rozšířit znalosti</span> objevováním článků o vzdálených částech světa?  Chtěl/a byste se naučit pohlížet na události <span class="bold">z jiné perspektivy</span>? Chtěl/a byste <span class="bold">rozvíjet růstové myšlení a sebereflexi</span>? <br><br>
Pokud jste odpověděl/a <span class="bold">ano</span> na většinu otázek, prosím klikněte zde <a href="lekce.html#cll">CLL lekce</a>.
</p>
<p><hr>!-->
1.	Chcete si procvičit <span class="bold">gramatiku a užití časů</span>? Toužíte lépe <span class="bold">mluvit a rychle rozumět</span>? Hledáte <span class="bold">flexibilní online výuku</span> nebo <span class="bold">výuku one-to-one</span>?<br>
Rád/a byste si vylepšil/a své znalosti angličtiny a potřebujete <span class="bold">zcela individuální</span> přístup?
<br><br>
Pokud jste odpověděl/a <span class="bold">ano</span> na většinu otázek, prosím klikněte zde <a href="lekce.html#individual">Individuální lekce</a>.

<p><hr>
2.	Chcete si procvičit <span class="bold">gramatiku a užití časů</span>? Toužíte lépe <span class="bold">mluvit a rychle rozumět</span>? <br>
Motivují Vás ostatní lidé? Mluvíte rád/a s různými lidmi v angličtině? Baví Vás <span class="bold">skupinová práce a práce ve dvojici</span>?
<br><br>
Pokud jste odpověděl/a <span class="bold">ano</span> na většinu otázek, prosím klikněte zde <a href="lekce.html#group">Skupinové kurzy</a>.




  </div></div>
  <div class="row2"><div class="block2">
  <h1 id="testimonials">Doporučení</h1><hr>
<h2>Akademické reference</h2>

<div class="refer"><p class="italic">„Tereza’s communication skills in English, both verbal and written, are excellent. She is also a highly motivated student who completes all assignments on time. The quality of Tereza’s work is also outstanding, as her excellent marks indicate.”</p>

<span class="bold">Dr. Steve Roe</span><br>

ENGL 100 (Academic Writing), Northern Lights College, Fort St. John, BC, Canada</div><hr>
<div class="refer">
<p class="italic">“Tereza is a very good writer, an excellent communicator and a diligent worker. In addition, she has made positive contributions to our work as a class. Her writing ability stretches across genres skillfully from short and long report writing and business memos to web presentations and peer feedback, breaking down complex ideas and concepts and stating them in clear, succinct prose… In addition to her writing and critical thinking skills, Tereza combines interpersonal aptitudes that make her easy to work with and she gives and receives feedback in a positive manner. She also demonstrates a continual willingness to help others with considerate, diplomatic comments on their work.”</p>

<span class="bold">John Vigna, MFA</span><br>

CMNS 251 (Professional Report Writing), University of the Fraser Valley, Abbotsford, BC, Canada</div><hr>




<h2>Profesní reference</h2>
<div class="refer">
<p class="italic">„Když jsem se rozhodla začít s angličtinou, kterou jsem ve škole nikdy neměla, vystřídala jsem několik
kurzů a několik lektorů, než jsem se seznámila s Terezou Warmanovou. Teprve s ní jsem zažila pocit,
že jsem konečně setřásla nálepku věčného začátečníka, že mě učení baví, že jsem se trochu víc
rozmluvila a že se angličtinu nebojím použít. Tereza je příjemná, trpělivá lektorka, schopná
přizpůsobit lekci žákovi, jeho schopnostem, ale i zájmům a momentální náladě. Nikdy nedá najevo
netrpělivost nebo rozmrzelost z opakujících se chyb. Na její hodiny se těším a odcházím pozitivně
naladěná, s chutí číst si knížky v angličtině a sledovat filmy bez dabingu.”</p>

<span class="bold">MUDr. Alena Mazáková</span></div><hr>
<div class="refer">
<p class="italic">„Na přístupu Terezy k výuce angličtiny si vážím hlavně toho, že poté co poznala, co na mě platí,
  tak se tomu přizpůsobila. Hodiny byly rozmanité a využité do maxima. Když jsme se nemohly sejít osobně, spojily jsme se přes skype -
  byla jsem překvapená, jaké finty na výuku na dálku má. Přestože jsem z časových důvodů (já vím, výmluva..) lekce přerušila, ráda si i
  nyní na facebooku čtu její sdílené příspěvky.”</p>

<span class="bold">Romana Musilová, AmbitionPro</span></div><hr> </div>

  </section>

  <footer>2018 © Tereza Warmanová-Vožniaková <br>Vytvořilo <a href="http://www.stuck-ups.com">Stuck-ups Web Designs</a></footer>
</div>
</body>
</html>
